# Lasagna

From David Newport with modifications by Jordan Newport

## Ingredients

-   1 lb ground meat (I recommend sausage)
-   12 lasagna noodles. Maybe one or two extra in case one disintegrates
-   1 onion, chopped
-   12 oz tomato paste
-   16 oz Mozzarella cheese
-   16 oz ricotta cheese (can substitute cottage cheese if you're concerned
about price)

-   Bay leaves, black pepper, Italian seasoning blend
-   Equipment: pot, pan, 9x13 baking dish

## Instructions

1.  Cook meat, drain grease
2.  Cook noodles
3.  Preheat oven to 375
4.  Add onion, tomato paste, 2 bay leaves, pepper, Italian seasoning, 2

cups of water to pan with meat; cook

1.  Arrange materials in baking dish: 2 layers each containing: 6 noodles

(3 stacks of 2), half the sauce, half of each kind of cheese

1.  Cook in oven for 30-35 minutes

