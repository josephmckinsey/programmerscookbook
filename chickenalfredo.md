# Chicken Alfredo

from Jordan's BSing

This recipe will focus solely on the chicken and the Alfredo stuff. Go to
[Spaghetti](spaghetti.md) for noodle instructions if necessary.


## Ingredients

-   Boneless, skinless, chicken tenderloin
-   Spaghetti or other pasta of some type.
-   Alfredo sauce

## Warnings

-   Keep raw chicken away from everything else to avoid spread of salmonella.
-   Use plastic cutting board so that it doesn't seep into the wood and spread
    salmonella.
-   Better to cook for a long time than a little, so that you kill the
    salmonella.
-   Have you noticed a trend? It's salmonella.

## Instructions

1.  Start boiling water around the time you start cutting water.
2.  Cut chicken into ~2.5 cc chunks. May take a while. Be wary of tendons.
3.  Heat pan with cooking oil (spray or vegetable oil). Use 7/10 heat.
4.  Put all chicken stuff onto pan.
5.  Flip often until brown on 7/10 heat with 1 minute per side (use your eyes,
    you fool!).
6.  Turn to 2-3/10 for 10 minutes. You want your chicken to be 170 degrees
    Fahrenheit all the way through (hence 2 to 3 / 10) so it kills all the
    salmonella.
7.  Prepare your pasta!
8.  Prepare your sauce. Look for sauce.org: [Sauce](sauce.md).
9.  Combine and stir pasta with sauce.

## Notes

### First Time 9/1/2018

Could taste tendons at some point, so be careful cutting it.

Cut chicken into 2.5 cc chunks instead of 4 cc chunks.

Consider making Alfredo sauce from scratch. And adding more cream or
something.

6 servings were created with one jar of 16 ounces (1 pound) and around 1.3
pounds of chicken and 16 ounces (1 pound) of pasta. 14 minutes for 10-12
spaghetti. We probably will have a serving or two of leftovers spread across
the both of us.

