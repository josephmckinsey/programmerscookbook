# Enchiladas

From David Newport and Jordan Newport

## Ingredients

-   1 pound ground beef (may use other ground meat)
-   1 small onion, chopped up
-   1 small can diced green chilies (mild or medium)
-   Taco seasoning
    -   Kroger brand taco seasoning = turquoise pack of powder. 2/3 cups of
        water.
-   Mission "soft taco" flour tortillas (8 inches in diameter)
-   Can of Old El Paso Mild Red Enchilada sauce (about 19 oz)
-   8-12 oz of cheddar cheese

## Equipment

-   9"x13"; baking pan

## Instructions

1.  Chop onion before cooking or while cooking ground beef.
2.  Cook ground beef, then drain grease.
3.  Add taco seasoning and 2/3 cups of water.
4.  Add the onion.
5.  Add the chilies and stir them up occasionally
6.  Let them cook on low heat for 10-15 minutes until the water is gone.
7.  Like tacos but with veggies too.
8.  Preheat the oven to 350 degrees F.
9.  Roll tortilla into narrow circle.
10. Place tortillas into mixture
11. Add to pan with the overlapping side down.
12. Err on the side of not enough mixture in the tortillas.
13. Repeat until out of space in the pan, should be about 7 tortillas.
14. Leftover mixture? Dump it on top. Not enough mixture? You fool! You
    misjudged it, now you will have to mash the tortillas down to fill the
    pan.
15. Spread the cheese across the tortillas.
16. Make sure you hit the sides so it will drip down and get into the
    tortillas.
17. Shake the can of sauce, open it, and pour it evenly across the top.
18. Put the pan in the oven for 30-35 minutes. When the cheese is melted and
    cooked, take out.
19. Eat.

## Sleazy Way

1.  Package of frozen burritos
2.  Cheese
3.  Enchilada sauce.
4.  Put the burritos in the pan.
5.  Cover with cheese and sauce.
6.  Bake for 35 minutes on 350 degrees.
7.  Eat.

