# Bratwurst (Sausages in general)

from David Churchwell's experiences on the grill. And some of Joseph McKinsey's.

Take everything with a grain of salt, since the time for grilling depends
heavily on your grill. Some grills will burn meat to a crisp within minutes.

## Ingredients

-   Sausages
-   Hot dog buns
-   Salt (optional seasoning)
-   Pepper (optional seasoning)
-   Olive oil (optional seasoning)
-   Cheese (optional)
-   Condiments (optional)

## Warnings

Don't burn yourself. Don't be afraid of the flames. Pretty simple.

## Instructions

1.  Start grill and set it to high on all settings.
2.  Season sausages by sprinkling some salt, some pepper, and then flipping it
    over and doing the same.
3.  Put some olive oil along the edges and massage it into the meat.
4.  Wait until it gets to medium to high temperature (400 degrees Fahrenheit
    if available).
5.  Place the sausages on the grill. Don't put it far back into the
    high-temperature zone, but rather in the middle to the front.
6.  Wait a few minutes (2-3) and flip them over on the grill. See how
    well-cooked it is.
7.  Wait a few more minutes and flip them again. Check how cooked it is.
8.  Place any cheese on the sausages (when there will no more flips). Grill
    until desired and then take them off and put them into the hot-dog buns.
9.  Let the meat rest for 5 minutes or so and then consume.


## Notes

### 9/8/2018

Got most of this from David, since he actually seasons stuff. I like them
just a tiny bit charred, but not everyone does, so a lower temperature may
help in that regard. I think they turned out fine this time.

