# Anri's Mac & Cheese

Anri (Girlfriend of Gabe (brother of Joseph))

## Ingredients

- 1 pound pasta
- 1 stick of butter (5 tablespoons)
- 2-3 cloves of minced garlic
- 2.5 tablespoons of flour
- 8oz Colby jack cheese (block)
- 12oz medium sharp cheddar (block)
- 4oz sharp cheddar (block)
- 8oz white cheddar (block)
- 6oz mozzarella (block)
- 2.5 tablespoons of cream cheese
- 1/2 tsp garlic powder
- 1/2 tsp of onion powder
- 3/4 tsp of paprika
- 1/2 tsp black pepper
- Salt to taste
- 3.5 cups of half-and-half
- 1/2 cup of cream

## Instructions

1. Melt 5 tablespoons of butter in a pot on medium heat
2. Add garlic for a couple of minutes. (stir)
3. Add 3.5 cups of half-and-half and 1/2 cup of cream and let simmer
4. Turn to low heat and add all the cheese slowly until melty. Add spices and cream cheese.
5. While this is happening, boil pasts and then add into the cheese sauce after step 3.
6. Optional: save 3 cups of the cheese to put on top and bake in the oven at 425 for 15 minutes.


# Notes

I tried to avoid boiling anything except the water and would purposefully take it off.
I also put the temperature around 4.7 on my stove.

I started the water to begin boiling on step 2.

There was way too much cheese. I ended up with lots of
cheese (like at least 2 cups). Even then I had too much cheese.
