# Chicken Korma

From dinnerthendessert.com

## Ingredients

-   6 chicken thighs boneless and skinless
-   2 tbsp tomato paste
-   1 tbsp sized piece of fresh ginger
-   2 cloves garlic
-   1 tbsp garam masala
-   1/4 tsp crushed red pepper
-   1/2 tsp paprika
-   1 tsp ground cardamom
-   1/2 tsp salt
-   1/2 tsp turmeric
-   1/2 cup unsalted raw almonds
-   3/4 cup (6 oz) Greek yogurt
-   1 tbsp canola oil
-   3 tbsp butter
-   1 yellow onion, diced
-   1/4 cup heavy cream

## Instructions

1.  Cut chicken into chunks and put them in a large bowl
2.  Mix tomato paste, ginger, garlic, garam masala, crushed red pepper,
    paprika, cardamom, salt, turmeric, and almonds in a food processor on high
3.  Add this mixture and the yogurt to the bowl with the chicken and mix
    well
4.  [Marinate](marinating.md) for 1-2 hours
5.  Add oil and butter to skillet on medium high heat
6.  Add onions and cook for 5-7 minutes until just caramelized
7.  Add chicken and cook for 12-15 minutes or until chicken is cooked
    through
8.  Add heavy cream to skillet and mix well, cooking for an additional 3-4
    minutes
9.  Serve with [Basmati rice](rice.md)and optionally naan bread

