# Home-made Pancakes

from Sandra McKinsey

## Ingredients

-   1 cup milk
-   1 egg
-   4 teaspoons of baking powder
-   1/4 teaspoons of salt
-   1 tablespoon of sugar
-   1 cup (unbleached if possible) all-purpose flour
-   2 tablespoons of vegetable oil
-   8 ounces of nestle semi sweet chocolate chips (optional)

## Instructions

1.  Mix all dry ingredients first.
    1.  4.5 teaspoons of baking powder
    2.  1/4 teaspoons of salt
    3.  1 cup of unbleached flour
    4.  1 tablespoon of sugar
2.  Whisk your egg in separate bowl. Then mix.
3.  Add 1 cup of milk.
4.  Add two tablespoons of oil.
5.  Add some chocolate chips (optional)
6.  Spray griddle or pan with some sort of cooking oil. Preheat (set to high
    normally, but then go to 4/10 once hot).
7.  Take 7/16 cup of your mix and pour it onto the griddle.
8.  Wait until you can see some layers of wrinkles forming on the bottom of
    the pancake. Then you should flip. Around 2 minutes at 4/10 heat.
9.  Wait again until some wrinkles form and then flip if you want to cook the
    first side more. Otherwise, take it off and enjoy. Less than 2 minutes at
    4/10 heat.

## Notes

### 9/9/2018

Tried using one of the pans, and some of them totally ended up burnt, sadly.
Others turned out great. In the future, I think I added too much vegetable
oil and maybe I should add some butter or something. Either that or it was
the bleached all-purpose flour.

On using the pan, I set it to high and heavily burned one. Then I set to 4
and they came out okay. I tried going a bit higher, but it burned pretty
fast. I think in the future I might even go lower so I can get a longer
spread.

I also was frustrated by the small pan-size. They ended up a bit too
compact overall. The chocolate chips didn't turn out quite like I wanted, since I
added them after I poured. In retrospect, I should have put the chocolate
chips in the measuring cup before pouring.

### 10/10/2018

Using 4.5 teaspoons of baking powder.

### 10/21/2018

Doubling and using 9 teaspoons of baking powder.

### 2/3/2018

Just use 3 tablespoons of baking powder from now on.

