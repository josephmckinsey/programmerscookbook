# Burgers

from David Churchwell, transcribed from his actions and steps.

Take everything with a grain of salt, since the time for grilling depends
heavily on your grill. Some grills will burn meat to a crisp within minutes.


<a id="orga60ca00"></a>

## Ingredients

-   Patties! (People Order Our Patties)
-   Burger buns
-   Cheese (optional)
-   Salt (optional seasoning)
-   Pepper (optional seasoning)
-   Olive oil (optional seasoning)
-   Lettuce (optional)
-   Tomatoes (optional)
-   Condiments (optional)

## Warnings

Don't burn yourself. Don't be afraid of the flames. Pretty simple.

## Instructions

1.  Start grill and set it to high on all settings.
2.  Season burgers by sprinkling some salt, some pepper, and some olive oil.
    Then flipping it over and doing the same.
3.  Wait until it gets to high temperature, then set to low.
4.  Before it get too hot, place all the buns on the top shelf (or toast
    inside in possible). Wait for < 1 minute and then take off. They should
    have some lines, but not be too burned.
5.  Place the burgers on the grill. Put it in the middle of the grill, but no
    quite on high-temperature zone, but rather in the middle to the front.
6.  Wait a few minutes (2-3) and flip them over on the grill.
7.  Wait a few more minutes and flip them again. You may have to flip it once
    more if on a lower heat.
8.  Place any cheese on the burgers. At this point, there may be quite a bit
    of flames. Then take them off in like a minute and a half.
9.  Put onto buns and add nothing for now.
10. Let the meat rest for 5 minutes or so.
11. Add tomatoes, lettuce, and condiments as desired.
12. Eat.


## Notes

### 9/8/2018

Got most of this from David, since he did all the work. He was a real pro at
all of it. He dislikes it when people set it to low or medium and just kind
of let it cook. This is exactly what my family does, so I guess that's that.

Andrew recommended 3 minute / 5 minutes / 3 minutes for being well done. I
assume that's on a much lower temperature than this one.

