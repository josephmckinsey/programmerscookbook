# Marinating

It's when you take some meat and put it in a liquid in a refrigerator to let
the flavor sink in.

## How Long?

20 minutes to 24 hours. (20 minutes or overnight).

## Typical Examples

-   Chicken with greek yogurt: [Chicken Tikka Masala](chickentikkamasala.md)
-   Barbecue sauce.
-   Milk.

Do you want it to taste like some liquid?

