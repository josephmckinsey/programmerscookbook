The Programmer's Cookbook
-------------------------

All the cooking recipes possible.


.. toctree::
    :maxdepth: 1
    :caption: Super Basic

    Spaghetti - simple, easy, why are you here? <spaghetti.md>
    rice.md
    marinating.md
    sauce.md

.. toctree::
    :maxdepth: 1
    :caption: Breakfast Baking

    fluffywaffles.md
    waffles.md
    pancakes.md

.. toctree::
    :maxdepth: 1
    :caption: Unhealthy Baking

    cookies.md

.. toctree::
    :maxdepth: 1
    :caption: More Healthy Baking

    biscuits.md
    creambiscuits.md

.. toctree::
    :maxdepth: 1
    :caption: Chicken Stuff

    chickenparm.md
    chickenpaprika.md
    chickenalfredo.md

.. toctree::
    :maxdepth: 1
    :caption: Lunch

    blt.md

.. toctree::
    :maxdepth: 1
    :caption: Grilling

    burgers.md
    bratwurst.md

.. toctree::
    :maxdepth: 1
    :caption: Mexican

    enchilada.md

.. toctree::
    :maxdepth: 1
    :caption: Indian

    chickentikkamasala.md
    chickenkorma.md

.. toctree::
    :maxdepth: 1
    :caption: Greek

    greekseasoning.md
    tzatzikisauce.md
    chickengyros.md

.. toctree::
    :maxdepth: 1
    :caption: American interpretations of Italian

    lasagna.md

.. toctree::
    :maxdepth: 1
    :caption: Anri's Mac and Cheese

    anrismacandcheese.md

.. toctree::
    :maxdepth: 1
    :caption: Chicken Fried Rice

    chickenfriedrice.md

.. toctree::
    :maxdepth: 1
    :caption: Potato-based Knish

    knish.md

.. toctree::
    :maxdepth: 1
    :caption: Baked Potato

    bakedpotato.md

.. toctree::
    :maxdepth: 1
    :caption: Roasted Broccoli

    roastedbroccoli.md
