# Baked Potato

From 
https://web.archive.org/web/20210310092138/https://www.allrecipes.com/recipe/54679/perfect-baked-potato/

## Ingredients

- 1 potato
- 1 teaspoon olive oil
- 1/2 teaspoon of salt
- 1/2 teaspoon of black pepper
- serve with butter

## Instructions

1. Preheat the oven to 350 degrees F
2. Put it in the water
3. Poke holes with fork all over
4. Salt and pepper it.
5. Rub it in olive oil
6. Wrap it in aluminum foil
7. Put it in the oven for 60 minutes.

# Notes

From a review
- 45 minutes at 400 degrees F. 60 minutes at 350 degrees F. 90 minutes at 325 degrees F. 

Although I might choose a little bit more. You should be able to poke it through.
