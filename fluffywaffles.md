# Extra Fluffy Waffles

from https://www.spendwithpennies.com/fluffy-homemade-waffle-recipe/

## Ingredients

- 2 cups flour
- 1 tablespoon baking powder
- 1/2 teaspoon salt
- 2 tablespoons sugar
- 2 eggs divided
- 1 2/3 cup milk
- 1/3 cup vegetable oil

## Instructions

1. Preheat waffle maker.
2. Mix flour, baking powder, sugar, and salt into a bowl.
3. In a small bowl, mix egg yolks, milk, and oil.
4. In another bowl, beat egg whites until "stiff peaks" form.
5. Add egg yolk mixture to flour mixture and stir to combine. Fold in egg whites.
6. Pour into waffle maker

## Notes

If you do not beat the egg whites, reduce milk to 1 1/2 cups, and add whole eggs to the butter mixture.

### Buttermilk Waffles

To make buttermilk waffles, reduce baking powder to 1 1/2 teaspoons and add 1/2 teaspoon baking soda. Replace milk with buttermilk.

### Personal Notes

You are going to need an electric whisker to beat those egg whites into submission.

