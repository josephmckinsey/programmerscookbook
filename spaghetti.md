# Spaghetti - simple, easy, why are you here?

from Joseph McKinsey

## Ingredients

-   Some sort of spaghetti things. Shells, elbow, whatever.
-   Water
-   Sauce pan. No cover needed.

## Instructions

1.  Pour 2 qts. for 1 person, 3 qts. for 2 people, or 5 qts. for 4 people, of
    water into the pot.
2.  Boil on high temperature.
3.  Pour spaghetti in. You may need to break spaghetti up to fit into the pan.
4.  Stir. Leave for a bit, stir again.
5.  Taste noodle in ~8-9 minutes.
6.  Take it off once done. Typically around 10-11 minutes myself.

