# Home-made Cookies!

from Sandra McKinsey

## Ingredients

-   Two eggs
-   2 1/4 cups of bread flour
-   1 cup of unsalted butter
-   3/4 cup granulated white sugar
-   3/4 cup packed down brown sugar
-   1 teaspoon baking soda
-   1 teaspoon of vanilla
-   3 tablespoons of whole milk
-   1/4 teaspoon salt
-   12 ounces of semi-sweet chocolate chips

## Instructions

1.  Beat 1 cup unsalted butter
2.  Add 3/4 cup unbleached bread flour.
3.  Add 3/4 cup granulated white sugar and 3/4 packed down brown sugar.
4.  Add 1 teaspoon baking soda
5.  1/4 teaspoon salt
6.  Preheat oven to 375 degrees Fahrenheit
7.  Add two eggs. One at a time. Room temperature or cold.
8.  Add 1 teaspoon vanilla
9.  Add 1 1/2 cups of bread flour
10. 3 tablespoons of whole milk
11. 3 ounces of semi-sweet chocolate chips. (or as desired).
12. Drop by tablespoon. Roll. Cook for 11 minutes.

## Notes

### 1st time making cookies 8/29/18

Typically want to cook for 11 - 12 minutes or maybe a bit longer for a
crunchier texture. At least in Golden.

Possible confounding factors include the fact that I added 3/4 cups of bread
flour right before brown sugar and after butter. I should have added
granulated white sugar. I added the white sugar later, but at the step when
I should have added bread flour. I then added the rest of the bread flour.

I also added a bit too much salt. They definitely aren't oversalted, but
still rather unnecessary in the end.

It resulted in 33 cookies of relatively large size (11 per pan).

The small chocolate chip size was also annoying. Should add more in the
future.

It would also be better to hold off on stirring too much, since I don't
think it helped too much, and the consistency was rather worrying.

The dough itself was a little too soft. I would like it to be firmer in
texture, so it loses less of it's shape during the baking process.

I didn't try doubling the batch yes, and I definitely didn't try browning
the butter. I also put the butter in the microwave first for 10 seconds,
then 10 seconds, then 15 seconds. So 20 - 25 seconds in one round should be
sufficient to soften.

I was also not satisfied with my first choice of small mixing bowl, so I
moved to a larger one. This was a good decision, but I lost quite a bit of
butter, and it took a long time to scoop it out.

Use terminal timer as well. Since Alexa is annoying and google had to
request permission.

They turned out well overall. Jack and Jordan give favorable reviews, but I
myself would like a little more chocolate chips, and perhaps be a bit
firmer.

Cleaning took like 45 minutes with everyone. But man does it sparkle.

With one cookie for each of us, we have 29 cookies left. 

### 2/7/2018

So, 11 minutes is really the best option for cookies.

Putting 3/4 cups of bread flour before putting in sugar is actually a great
idea. When I've cooked them that way, they have turned out better, but it
could be access to better tools like a more precise oven.

When softening the butter, I have previously just microwaved it a bit and
then beat it. This is problematic since buttery chunks persist without heavy kneading.

The great amount of kneading may also have contributed.

I also added a bit more vanilla than usual, as well as 3 tablespoons of milk instead of just 2.

I got 32 cookies out of it as well.

