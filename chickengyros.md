# Chicken Gyros

From www.daringgourmet.com

## Ingredients

-   1-1.5 pounds chicken, cut into strips
-   3-4 tbsp [Greek Seasoning](greekseasoning.md)
-   2 tbsp extra virgin olive oil
-   1/2 medium yellow onion, thinly sliced
-   Possible toppings include: lettuce, cabbage, chopped tomato
-   Pita bread (6-8 regular sized ones)
-   [Tzatziki Sauce](tzatzikisauce.md)

## Instructions

1.  Add seasoning to meat, toss to combine, and marinate for at least 15
    minutes. This is a good time to cut your onion and prepare your toppings.
2.  Heat the oil in a skillet over medium-high(7/10) heat. When hot, add
    chicken and cook until all sides are golden brown (this is slightly more
    cooking than is common for other recipes). Add onions and continue to cook
    until the onions are translucent, about 5-7 minutes. You can add more oil or
    seasoning if you feel it is appropriate.
3.  While this is happening, wrap the pitas in foil and warm them in the
    oven.
4.  Serve on pitas with sauce and any toppings.

