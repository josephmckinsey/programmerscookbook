# Cream Biscuits

From <https://www.thekitchn.com/how-to-make-cream-biscuits-109151> originally.
Modifications from Joseph McKinsey

Makes 8 biscuits

## Ingredients

-   2 cups of all-purpose flour
-   2 1/2 teaspoons of sugar
-   5.5 teaspoons of baking powder
-   3/4 teaspoons of salt
-   1 1/2 heavy cream (divided)
    -   Milk is a good substitute if you run out.

## Equipment

-   Parchment paper - may be unnecessary.

## Instructions

1.  Prep and preheat: Place a layer of parchment paper across the bottom and
    up 2 sides of an 8x8-inch pan. Preheat oven to 425°F. May work without
    parchment just fine.
2.  Mix the dry ingredients: In a medium (large preferred over small) bowl,
    mix together dry ingredients until combined.
3.  Add the cream: Pour in all but 1/4 cup of the cream. Stir until the dough
    is well-mixed and clumping, then add the remaining cream and stir to
    combine.
4.  Well-flour on sheet so you can need and cut into it on that.
5.  Knead the dough: Turn the dough out onto a well-floured work surface.
    Knead the dough for about 30 seconds, just until it all comes together.
6.  Form the dough: Shape dough into a rectangle, about 12 inches long and 4
    inches wide. Cut it in half lengthwise, then cut each piece into 4 pieces
    horizontally. Place each of the pieces in the prepared pan. You want your
    pieces around 1" tall. High is the only thing that actually matters.
7.  Bake: Transfer the pan to the oven, and bake for around 18.5 minutes, until golden.

## Notes

### 2018-09-18

Added some more baking powder and had to substitute milk at the end. The use
of cream was nice, but I think that it needed to be taller. It think I should
try using the cream of tartar and more baking powder. Perhaps bread flour? It
needs to be taller and cook better on top. I'm not sure about that.

I'm not happy with how it cooked on the top, so I'll have to try
adjusting that later.

I think we should try and bake it hotter for a more golden brown at 450.

### 2018-09-30

Two tablespoons of bread flour
2.5 extra teaspoons of baking powder
1/3 cup of milk and 1 1/4 cup of cream

Set first timer at 13 minutes
Set another 3 minute timer.

### 2019-01-13

Used 5.5 teaspoons of baking powder

Temperature at 430.
Set timer for 17 minutes, then waited an extra minute or two to brown.
