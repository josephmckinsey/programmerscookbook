# Biscuits (High-Altitude)

From Sandra Hanna's cookbook with modifications.

20 biscuits

## Ingredients

-   2 cups all-purpose flour (unbleached)
-   1 cup whole wheat flour
-   4 1/2 teaspoons baking powder
-   1/4 teaspoon salt
-   3/4 teaspoon cream of tartar (powder).
-   3/4 cups butter, softened and cut into pats (may use 1/2 cup)
-   1 egg, lightly beaten
-   1 cup milk

## Instructions

1.  Combine dry ingredients: flours, baking powder, sugar, salt and cream of
    tartar.
2.  Use pastry blender, cut butter into dry ingredients until mixture
    resembles coarse meal.
3.  Add egg and milk, stirring until smooth.
4.  Knead dough on lightly floured board.
5.  Roll dough out to 1&ldquo; thickness and cut into 2-inch biscuits.
6.  Place in greased 9-inch square pan. (not really necessary).
7.  Bake at 450 degrees for 12-15 minutes or until golden brown.

