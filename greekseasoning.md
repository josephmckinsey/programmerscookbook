# Greek Seasoning

From www.daringgourmet.com.

Makes twice as much as you need, but it's not like you have a 1/8 tsp
laying around. Just cook the dish you need this for again.

## Ingredients

-   1.5 tbsp oregano
-   1.5 tsp salt
-   1 tbsp onion powder
-   1 tsp black pepper
-   2 tsp beef bouillon powder (optional; hard to find)
-   2 tsp dried parsley flakes
-   1 tsp dried thyme
-   1.5 tbsp paprika
-   1/4 tsp ground cinnamon

## Instructions

1.  Combine all spices; mix thoroughly.

