# Potato and Onion Knish

From https://www.tasteofhome.com/recipes/knish/

## Ingredients

- 2-1/4 cups all-purpose flour
- 1 teaspoon baking powder
- 1/2 teaspoon salt
- 1/2 cup cold butter, cubed
- 3 ounces sour cream

### FILLING
- 1 pound medium potatoes, peeled and cubed (about 2 cups)
- 1/4 cup butter, cubed
- 2 medium onions, finely chopped (maybe 1 large one (maybe))
- 2 large eggs, lightly beaten, divided
- 1 teaspoon salt
- 1/4 teaspoon pepper


## Instructions

- For pastry, in a large bowl, combine flour, baking powder and salt. Cut in butter until crumbly.
Stir in sour cream, adding 3-4 tablespoons water to form a dough.
Shape into a disk, mixture will be crumbly.
Wrap and refrigerate at least 2 hours or overnight.

- Place potatoes in a large saucepan; add water to cover.
Bring to a boil. Reduce heat; cook, uncovered, until tender, 8-10 minutes.

- Meanwhile, in a large skillet, melt butter over medium-high heat.
Add onions; cook and stir until tender, 8-10 minutes.

- Drain potatoes; return to pan and stir over low heat 1 minute to dry.
Mash potatoes; stir in onion mixture, salt and pepper. Set aside to cool. Stir in eggs.

- Preheat oven to 400°. On a lightly floured surface, roll dough into a 10x12 -in. rectangle.
Cut into 16 squares. Spoon 1 tablespoon potato filling in the middle of each square.
Brush the edges with water. Fold each corner toward the center, meeting in the middle.
Arrange, seam side down, on ungreased baking sheets. Bake until lightly browned, 15-20 minutes.


# Notes

- I wish I had a rolling pin.
- I always feel like I have too much onion and not enough potato.
- The sourcream is important.
- REMEMBER TO REFRIGERATE AFTERWARDS.
- Dicing the onion is quite hard. Mashing is annoying if you mess up the potato, so focus on that.
