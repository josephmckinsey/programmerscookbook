# Chicken Fried Rice

https://www.cookingclassy.com/chicken-fried-rice/

## Ingredients

- 3 cups cooked long-grain white/brown rice (preferably left over rice)
- 3/4 lb bonelss skinless chicken breasts, diced into 3/4-inch pieces
- 1 Tbsp toasted sesame oil
- 1 Tbsp canola oil (or olive oil in a pinch)
- 1 1/3 cups frozen peas and carrots blend
- 3 green onions chopped
- 2 cloves minced garlic
- 2 large eggs
- 3 Tbsp low-sodium soy sauce (or 2 Tbsp soy sauce with basically no other salt).
- Salt and black pepper

## Instructions

1. In a large non-stick wok or skillet, heat 1 1/2 tsp sesame oil and 1 1/2 tsp of the
canola oil over medium-high heat. Once hot, add chicken pieces, season lightly with
salt and pepper and saute until cooked through, about 5 - 6 minutes. Transfer
chicken to a plate and set aside.
2. Return skillet to medium-high heat, add remaining 1 1/2 tsp sesame oil and 1 1/2 tsp canola oil.
Add peas and carrots blend and green onions and saute 1 minute, then add garlic and
saute 1 minute longer. Push veggies to edges of pan, add eggs in center
and cook and scramble.
3. Return chicken to skillet along with rice. Add in soy sauce and season with
salt and pepper to taste. Serve warm with Sriracha to taste if desired.

# Notes

- Often can easily end up too salty.
- Is two man meals?

