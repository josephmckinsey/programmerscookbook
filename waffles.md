# Home-made Waffles

from Scott McKinsey

## Ingredients

-   2 cups all-purpose flour
-   1 teaspoon salt
-   4 teaspoons baking powder
-   2 tablespoons white sugar
-   2 eggs
-   1 1/2 cups milk

## Instructions

1.  Whisk eggs, milk, salt, sugar, and baking powder together.
2.  Using mixer of large spoon, mix the flour in.
3.  Resulting consistency should pour like honey. Add milk or flour as needed.
4.  Pour into waffle-maker or griddle and wait around 5 minutes, or until brown.

