# Basmati Rice

from Unknown

## Ingredients

-   1 cup Basmati rice (or any other long-grained rice).
-   2 cups of water
-   1/2 teaspoon of salt
-   1/2 teaspoon of butter (optional)
-   1/2 teaspoon of white vinegar (optional)

## Instructions

1.  In a saucepan with a good fitting lid, put water, salt, and optional
    ingredients to boil.
    -   salt and butter for flavor
    -   vinegar mostly to keep grains separated and presentation
2.  Add rice and stir. 1 cup of rice for every 2 cups of water.
3.  Bring temperature down to 4/10 or 3/10.
4.  Cover with well-fitting lid.
    -   Should see a little bit of steam leaking from pot. This is good. Too
        much steam indicates you should turn it down.
5.  Cook for 20 minutes.
6.  DO NOT LIFT LID! Trapped steam is absorbed into rice.
7.  Remove from heat and fluff.

## Notes

### 9/3/2018

Not more than 20 minutes, even in Colorado's high elevation. More rice will be
burned on the bottom. In the future, err on the side of shorter cooking times.

1 teaspoon of butter is too much. Taste of butter overwhelms salt and rice. I
would suggest 1/2 or even 1/3 max. Same with salt. You can always salt later.

3/10 may be more reasonable than 4/10. Will test in future.

1 cup is still a good metric. Perhaps put it in packet form?

