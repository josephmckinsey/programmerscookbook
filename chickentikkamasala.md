# Chicken Tikka Masala

From <https://www.bowlofdelicious.com/easy-chicken-tikka-masala/>. Note that it
is significantly more difficult than it lets on.

## Ingredients

### Rice section

-   [Basmati Rice](rice.md) - original recipe recommends 1 cup
-   1 tablespoons butter or olive oil
-   1/2 teaspoon cumin seeds (optional)
-   2 cups water
-   1/2 teaspoon salt

### For the Chicken:

-   1 lb. chicken breasts, cut into 1-inch cubes
-   1/2 cup Greek yogurt plus 1 teaspoon garam masala (optional)
-   salt and pepper, to taste
-   1 tablespoon olive oil
-   4 tablespoons butter
-   1/2 onion, diced
-   1-inch piece of ginger, grated\*
-   2 cloves garlic, minced
-   1 cups (1 small can) crushed or pureed tomatoes
-   1 heaping tablespoon garam masala
-   1/4 teaspoon cayenne pepper (optional)
-   3/4-1 cup heavy cream or whole milk (coconut milk can also be used)
-   Naan, for serving (optional)

## Instructions

1.  If you have the time, [marinate](marinating.md) the chicken in the yogurt, 1 teaspoon
    garam masala, and salt and pepper to taste for 20 minutes or overnight.

2.  For the rice, see [Basmati Rice](rice.md) recipe. I would recommend melting
    everything but the rice first, then putting it in and set to a 3. Note
    that they used brown basmati rice while we used white.
    
    Original recipe recommends to melt butter or olive oil in a medium pot
    over medium high heat. Add rice and cumin seeds and toast for
    approximately 3 minutes, stirring occasionally. Add water and salt, bring
    to a boil, cover and turn heat to low. Rice will cook in approximately 30
    minutes.

3.  While rice cooks, heat 1 tablespoon olive oil plus 1 tablespoon butter in
    a large skillet or pot over medium-high heat.

4.  Cut everything you need before you need it. Prepare it first, then cook it.
    -   Make sure to cut everything else first before the chicken. Or clean
        immediately after the chicken.

5.  If you did not marinate the chicken, season it with salt and pepper.
    Brown chicken until fully cooked on both sides. Remove from pot and set
    aside.

6.  Add remaining 3 tablespoons butter to pot and saute onions and ginger
    until softened.

7.  Add garlic and saute for approximately 1 minute, or until fragrant.

8.  Add tomatoes, garam masala, cayenne pepper (optional), and chicken.

9.  Once heated, add cream or milk.

10. Serve over rice with naan.

## Notes

### 9/28/2018

-   Half a onion or a really small one.
-   Slightly less tomato.
-   More cream.
-   Don't toast the rice if it's white. Cook rice normally with some extra
    dishes: [Basmati Rice](rice.md)
-   Also more rice if a rice fan is eating. (Joseph)
-   Naan bread is best. Don't make it yourself from scratch.

