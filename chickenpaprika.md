# Chicken Legs with Lots of Paprika

From https://www.julieseatsandtreats.com/baked-chicken-legs/ (recommended by Sandra Hanna)

## Ingredients

- 8 Chicken drumsticks
- 2 Tablespoons of Olive oil
- 1 Teaspoon Sea salt
- 0.5 Teaspoon of Black pepper
- 1 Teaspoon of Garlic powder
- 1.5 Teaspoons of Smoked paprika
- 0.25 Teaspoons of Chili powder
- 1 Teaspoon of Onion powder

## Instructions

1. Thoroughly dry the chicken first. Use paper towels to pat them dry – removing the excess moisture from the skin will make it easier to get that perfectly crispy skin!
2. Put the drumsticks in a large resealable bag along with the olive oil. Seal the bag and toss to coat the chicken in the oil.
3. Add all of the seasoning, seal the bag again and toss again to coat the legs evenly with the spices.
4. For easy clean-up I like to line my baking sheet with foil. Go ahead and lightly coat it with cooking spray to keep the drumsticks from sticking while they bake.
5. Bake at 425 degrees. Bake them uncovered on the baking sheet – you don’t have to turn them, just leave them to cook and after about 40 to 45 minutes they’ll be ready!


## Notes

- Baked chicken should be cooked to a temperature of at least 165 degrees, but for legs I let them cook longer. Sometimes there are red spots in the meat near the bone and if you cook them a little longer those disappear.

