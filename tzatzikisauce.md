# Tzatziki Sauce

From www.daringgourmet.com.

If you don't like sauce, this recipe will make you way too much. Can be
halved.

## Ingredients

-   1 cup (8 oz) plain Greek yogurt
-   1 tbsp lemon juice
-   1 tbsp dill
-   1 large clove garlic, minced
-   1/2 tsp salt
-   1/2 cup cucumber, very finely minced (This is approximately half a
    cucumber)

## Instructions

1.  Combine all ingredients in a bowl
2.  Refrigerate for at least an hour before use

