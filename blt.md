# BLT (with optional avocados)

from Dave Florness from the restaurant Colorado plus 49 Cidery and Pub.

## Ingredients

-   2 slices of bread
-   Jalapeño Aioli (sauce)
-   Lettuce
-   Tomatoes
-   Bacon
-   Avocados
-   Some type of oil (vegetable, canoli, or olive oil).

## Instructions

1.  Pour oil onto flat iron at high temperature (?).
2.  Put two slices of bread down.
3.  Put bacon on flat iron to get it nice and crispy.
4.  Once bread is crunchy, take off.
5.  Put Jalapeño Aioli on one side.
6.  Put Lettuce on top of that.
7.  Put Tomatoes on top of that.
8.  Then bacon.
9.  Then avocados.
10. Combine bread.
11. Cut in half to eat easier.

