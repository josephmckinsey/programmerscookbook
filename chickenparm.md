# Chicken Parm

From https://www.youtube.com/watch?v=p-LY9b1u_io&t=3s

## Ingredients

- Salt
- Pepper
- chopped garlic or garlic powder if lazy
- Olive oil
- 3/4 cups of of flour
- One egg, beaten
- Two cups of Panko
- Parmesan or pecorino cheese
- At least 4 slices of Mozarella

## Instructions
1. Cut the chicken breasts to half their original thickness, yielding four equal pieces. Pound them out on both sides, ideally with a spiked meat mallet. Season both sides liberally with salt and pepper. Put the chicken in a bowl with some of the chopped garlic and just enough white wine or olive oil to coat. Toss and refrigerate.
2. Prepare to bread the chicken by putting the flour and breadcrumbs onto separate plates and the beaten egg in a bowl. Grate a large pile of cheese onto the breadcrumbs and toss to mix it in. Dry the marinade off the chicken on paper towels. There's no need to go crazy with breakcrumbs and cheese
3. Coat each piece of chicken in flour, then egg, then breadcrumbs/cheese.
4. Pour a heavy coating of olive oil into a wide pan on medium heat. Fry the chicken gently, two pieces at a time, until golden on both sides and the internal temperature reads 160 F — 6-8 minutes. Remove cooked chicken to a cooling rack. You’ll probably need to add more olive oil for the second batch.
5. Place the chicken in the oven with a slice of mozarella on it. I use 400 F for 10 minutes to finish it off.
6. Make spaghetti.

## Notes

- Baked chicken should be cooked to a temperature of at least 165 degrees, but for legs I let them cook longer. 
