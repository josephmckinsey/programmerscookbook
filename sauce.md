# Sauce

from the back of the jars

## Ingredients

-   Sauce of your choosing
    -   Alfredo!
    -   Pesto (doesn't have to be cooked usually)
    -   Marinara!
    -   ???

## Instructions

### Microwave

1.  Alfredo

    Pour contents into a microwave-safe dish and cover loosely with plastic
    wrap. Heat on high (10/10) about 3 minutes, stirring once. Use potholders.

### Stove Top

1.  Alfredo

    Pour contents into a saucepan. Warm over medium heat. Stirring
    occasionally. Do not allow to boil. Cook until heated through.

